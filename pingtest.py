import datetime
import os
import time
import signal

shutdown_signal = False

def ctrlc_handler(signal, frame):
  print("\nCtrl-C detected. Shutting down safely")
  global shutdown_signal
  shutdown_signal = True

signal.signal(signal.SIGINT, ctrlc_handler)

def sleep_interruptible(secs):
  interval = 0.1
  for _ in range(round(secs/interval)):
    if shutdown_signal: break;
    time.sleep(interval)

def ping(addr):
  return os.system("ping -c 1 " + addr)

def write_row(f, data):
  f.write(",".join(str(datum) for datum in data) + "\n")

hosts = {
  "Google":   "google.com",
  "Facebook": "facebook.com",
  "YouTube":  "youtube.com",
  "Yahoo":    "yahoo.com",
  "router":   "192.168.1.1",
}

filename = "pingdata.csv"

if not os.path.isfile(filename):
  with open(filename, "w+") as pingfile:
    write_row(pingfile,
        ["Date+Time", "Host Name", "Host Address", "Return Code"])

while not shutdown_signal:
  for hostname, addr in hosts.items():
    current_time = datetime.datetime.now().isoformat()
    pingret = ping(hosts[hostname])
    print("\n\n")
    with open(filename, "a") as pingfile:
      write_row(pingfile, [current_time, hostname, addr, pingret])

  sleep_interruptible(15)
